package com.example.demo.model

import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table
class Student {
    @Id
    @SequenceGenerator(
            name = "student_sequence",
            sequenceName = "student_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "student_sequence"
    )
    var id: Long = 0
    var name: String = ""
    var email: String = ""
    var dob: LocalDate = LocalDate.now()
    var age: Int = 0

    constructor() {

    }

    constructor(id: Long, name: String, email: String, dob: LocalDate, age: Int) {
        this.id = id
        this.name = name
        this.email = email
        this.dob = dob
        this.age = age
    }
}