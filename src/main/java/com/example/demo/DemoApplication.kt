package com.example.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.GetMapping
import kotlin.jvm.JvmStatic
import org.springframework.boot.SpringApplication
import com.example.demo.DemoApplication
import com.example.demo.model.Student
import java.time.LocalDate
import java.util.ArrayList

@SpringBootApplication
@RestController
open class DemoApplication {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(DemoApplication::class.java, *args)
        }
    }
}