package com.example.demo.student

import com.example.demo.model.Student
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface StudentRepo: JpaRepository<Student, Long>