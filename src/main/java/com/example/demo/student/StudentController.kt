package com.example.demo.student

import com.example.demo.model.Student
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
@RequestMapping(path = ["/api/v1/student"])
class StudentController {
    val studentService: StudentService

    @Autowired
    constructor(studentService: StudentService) {
        this.studentService = studentService;
    }

    @GetMapping()
    fun getStudents(): List<Student> {
        return studentService.getStudents()
    }
}