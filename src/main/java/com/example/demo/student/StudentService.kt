package com.example.demo.student

import com.example.demo.model.Student
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import java.time.LocalDate

@Service
open class StudentService @Autowired constructor(private val studentRepo: StudentRepo) {


    // Student(1L, "th", "th@gmail.com",
    // LocalDate.of(2000, 11, 11), 21)
    fun getStudents(): List<Student> {
        return this.studentRepo.findAll()
    }
}